package com.luban.ziya.debug;

import com.luban.ziya.adjust.CountObjectSize;
import com.luban.ziya.oom.HeapOverFlowTest3;
import org.openjdk.jol.info.ClassLayout;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created By ziya
 * 2020/12/30
 */
public class Test_1 {
    short s = 10;
    int i = 20;
    long l = 10;

    public static void main(String[] args) throws InterruptedException {
        CountObjectSize object = new CountObjectSize();

        System.out.println(ClassLayout.parseInstance(object).toPrintable());
    }
}