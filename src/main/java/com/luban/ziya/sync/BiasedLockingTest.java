package com.luban.ziya.sync;

import org.openjdk.jol.info.ClassLayout;

import java.util.concurrent.TimeUnit;

/**
 * Created By ziya
 * 2020/10/21
 */
public class BiasedLockingTest {

    static Object o = new Object();

    public static void main(String[] args) throws InterruptedException {
        TimeUnit.SECONDS.sleep(5);

        synchronized (o) {
            System.out.println(ClassLayout.parseInstance(o).toPrintable());

            synchronized (o) {
                System.out.println(ClassLayout.parseInstance(o).toPrintable());
            }
        }

        while (true);
    }

    public static void test() {
        synchronized (o) {
            System.out.println(ClassLayout.parseInstance(o).toPrintable());

            synchronized (o) {
                System.out.println(ClassLayout.parseInstance(o).toPrintable());
            }
        }
    }
}
